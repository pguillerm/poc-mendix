= POC Mendix
:toc:

---
== Contexte



---
== Points à vérifier 

=== Technologie

==== Mobile
Les applications mobile sont réelement développer en `react-native` avec `PhoneGap`. Ce framework permet comme pour
`angular` avec `ionic` de générer des applications mobile. 

Il est à noter que la compilation des APK nécessiste un compte chez Adobe PhoneGab

Cependant la partie build process est masqués en local. Il est donc relativement complexe de savoir comment
est packagé l'application.

==== Web
Les technologies côté web sont plus anciennes que celle côté mobile. On retrouve donc du `dojo` 
en framework principal côté WEB. Ce framework ne s'illustre pas par sa rapidité d'exécution.  Les pages sont décritent en XML avant d'être digérées par les API front 
de Mendix.

[source,xml]
----
<?xml version="1.0" encoding="utf-8" ?>
<m:page id="7b22940f-b2dd-43e1-9a0b-25011a7e1b33" xmlns="http://www.w3.org/1999/xhtml" title="Homepage" class="layout-atlas layout-atlas-responsive-default page-dashboard page-dashboard-actions" xmlns:m="http://schemas.mendix.com/forms/1.0">
    <m:layouts>
        <m:layout path="Atlas_UI_Resources/Atlas_Default.layout.xml"></m:layout>
    </m:layouts>
    <m:arguments>
        <m:argument parameterName="5b4049a6-0cf1-4a92-82ca-0e6b43ff1354">
            <div data-mendix-id="0_0" data-mendix-type="mxui.widget.ReactWidgetWrapper" data-mendix-props=""widgetTree":[{"widget":"Container","widgetId":"0_0","props":{"name":"container1","class":"mx-name-container1 pageheader","renderMode":"div","content":[{"widget":"Div","widgetId":"0_1","props":{"name":"layoutGrid2","class":"mx-name-layoutGrid2 mx-layoutgrid mx-layoutgrid-fluid container-fluid","content":[{"widget":"Div","widgetId":"0_2","props":{"name":"","class":"row","content":[{"widget":"Div","widgetId":"0_3","props":{"name":"","class":"col-md-12","content":[{"widget":"Text","widgetId":"0_4","props":{"name":"text1","class":"mx-name-text1 pageheader-title spacing-outer-bottom","caption":{"$type":"DynamicTextProperty","template":{"elements":["Home"],"friendlyId":"NativeMobile.Home_Responsive.text1"}},"renderMode":"h3"}},{"widget":"Text","widgetId":"0_5","props":{"name":"text2","class":"mx-name-text2 pageheader-subtitle text-detail","caption":{"$type":"DynamicTextProperty","template":{"elements":["Welcome to your new app"],"friendlyId":"NativeMobile.Home_Responsive.text2"}},"renderMode":"p"}}]}}]}}]}}]}}]"></div>
            <div data-mendix-id="0_6" class="mx-layoutgrid mx-layoutgrid-fluid container-fluid mx-name-layoutGrid1">
                <div class="row">
                    <div class="col-md-12">
                        <div data-mendix-id="0_7" class="mx-layoutgrid mx-layoutgrid-fluid mx-name-layoutGrid3">
                            <div class="row">
                                <div class="col-md-3"></div>
                                <div class="col-md-9">
                                    <div data-mendix-id="0_8" data-mendix-type="mxui.widget.ListView" data-mendix-props=""friendlyId":"NativeMobile.Home_Responsive.listView1","hasSearch":false,"datasource":{"friendlyId":"NativeMobile.Home_Responsive.listView1","type":"microflow","microflow":"NativeMobile.get_products","path":"NativeMobile.Feature","queryId":"2G/9G7jo802BZfMe+WLK6g","argMap":{}},"page":10,"selectable":"","action":{"type":"doNothing","argMap":{},"config":{}},"templateMap":{"NativeMobile.Feature":"NativeMobile.Feature"}" class="mx-name-listView1"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </m:argument>
    </m:arguments>
    <m:templates>
        <m:template widget-id="0_8" name="NativeMobile.Feature">
            <div data-mendix-id="0_9" data-mendix-type="mxui.widget.DataView" data-mendix-props=""friendlyId":"NativeMobile.Home_Responsive.listView1","entity":"NativeMobile.Feature","schema":"92f51bef-b973-478a-ae54-470bc247e6e7","readOnly":true,"datasource":{"friendlyId":"NativeMobile.Home_Responsive.listView1","type":"direct","path":"NativeMobile.Feature"},"conditions":{}" tabindex="0"></div>
        </m:template>
        <m:template widget-id="0_9" name="content">
            <div data-mendix-id="0_10" data-mendix-type="mxui.widget.ReactWidgetWrapper" data-mendix-props=""widgetTree":[{"widget":"Text","widgetId":"0_10","props":{"name":"text3","caption":{"$type":"DynamicTextProperty","scope":"0_8","template":{"elements":[1],"friendlyId":"NativeMobile.Home_Responsive.text3","parameters":{"1":"NativeMobile.Feature/description"},"formats":{"1":{}}}},"renderMode":"span","class":"mx-name-text3"}}]"></div>
            <div data-mendix-id="0_11" data-mendix-type="mxui.widget.ReactWidgetWrapper" data-mendix-props=""widgetTree":[{"widget":"Text","widgetId":"0_11","props":{"name":"text4","caption":{"$type":"DynamicTextProperty","scope":"0_8","template":{"elements":[1],"friendlyId":"NativeMobile.Home_Responsive.text4","parameters":{"1":"NativeMobile.Feature/title"},"formats":{"1":{}}}},"renderMode":"span","class":"mx-name-text4"}}]"></div>
            <div data-mendix-id="0_12" data-mendix-type="mxui.widget.ReactWidgetWrapper" data-mendix-props=""widgetTree":[{"widget":"Text","widgetId":"0_12","props":{"name":"text5","caption":{"$type":"DynamicTextProperty","scope":"0_8","template":{"elements":[1],"friendlyId":"NativeMobile.Home_Responsive.text5","parameters":{"1":"NativeMobile.Feature/uid"},"formats":{"1":{}}}},"renderMode":"span","class":"mx-name-text5"}}]"></div>
        </m:template>
    </m:templates>
</m:page>
----


=== Build process
Le build process de Mendix est assez ancien, il se base sur ANT. Le script de compilation est relativement
basic (clean,checkDependencies, compile, deploy). Il ne faut cependant pas modifier ce fichier, car il est 
générer par l'IDE de Mendix: Il n'est donc pas possible de modifier le comportement du build process.


=== JDK
Mendix est compatible Java 11, Cependant lors du POC certaines dépendances sont en Java 13, rendant la compilation
impossible. Il serait nécessaire de creuser la question pour gérer correctement les dépendances afin de compiler
correctement le projet.

image::doc/images/version_java.png[img,800,600]

=== Contribution à la charte graphique
Par défaut Mendix fonctionne avec la version `3.3.4` de bootstrap (la dernière version de bootstrap est la `4.3.1`).

Cette version de bootstrap n'est pas exempt d'anomalie. Sa prise en charge est assez variante suivant les navigateurs. 

image::doc/images/navigateurs.png[img,800,600]


Il est possible depuis le projet exporté de modifier le thème WEB en CSS. 

image::doc/images/theme_01.png[img,800,600]

La partie Mobile utilise du code JavaScript pour effectuer sa mise en page.

image::doc/images/theme_02.png[img,800,600]


=== Sécurité

=== Modèle de données et développement

==== Mapping des entités 

===== Pas d'importation depuis une définition Swagger
Dans l'IDE de Mendix il est possible d'import une définition d'un model objet depuis une `WSDL`. Cependant dans les
services `SOAP` sont de moins en moins utiliés pour les échanges inter-serveurs. La plus part des API modernes 
utilisent du `JSON` avec une exposition de la définition des services via `Swagger`. 

image::doc/images/import-wsdl.png[img,800,600]

Cependant lors de la créaction d'un consommateur `REST` il est possible de générer automatiquement le model en copiant
la structure d'exemple de `swagger`. Il faut par contre bien penser à renomer les entités générés pour pouvoir se 
repérer.

===== Pas de List<String> possible dans les entités
Il est possible de gérer des énumérations mais ces dernières sont statiques. Les API contenant ce type de variable ne 
sont donc pas utilisables. Si c'est une API que l'on développe il est possible d'adapter le model afin qu'il soit 
géré par Mendix. Dans le cadre d'API externe, il est donc nécessaire d'avoir une application proxifiant l'API afin 
adapter le schéma des données pour Mendix.

https://forum.mendixcloud.com/link/questions/91754

image::doc/images/list_string.png[img,800,600]




===== Extension en Java à utiliser avec précaution :
La structuration du code générer est assez particulière. Le build process est effectué avec `ant`. Il n'y a pas 
de gestionaire de dépendances. Les librairies tiers sont à rajouter directement dans le projet (`${model}/lib/userlib`).
Le script effectue une vérification sur les conflits potentiels entre dépendances via `jdeps`.

Si on souhaite pouvoir gérer proprement ses dépendances Java, il serait nécessaire de concevoir
un `lifecycle` spécifique dans maven et ainsi gérer et piloter le build process Mendix.

[source,java]
----
// This file was generated by Mendix Studio Pro.
//
// WARNING: Only the following code will be retained when actions are regenerated:
// - the import list
// - the code between BEGIN USER CODE and END USER CODE
// - the code between BEGIN EXTRA CODE and END EXTRA CODE
// Other code you write will be lost the next time you deploy the project.
// Special characters, e.g., Ã©, Ã¶, Ã , etc. are supported in comments.

package nativemobile.actions;

import com.mendix.systemwideinterfaces.core.IContext;
import com.mendix.webui.CustomJavaAction;
import com.mendix.systemwideinterfaces.core.IMendixObject;

public class convertToString extends CustomJavaAction<java.lang.String>
{
	private IMendixObject __value;
	private nativemobile.proxies.Features value;

	public convertToString(IContext context, IMendixObject value)
	{
		super(context);
		this.__value = value;
	}

	@java.lang.Override
	public java.lang.String executeAction() throws Exception
	{
		this.value = __value == null ? null : nativemobile.proxies.Features.initialize(getContext(), __value);

		// BEGIN USER CODE
		return String.valueOf(value); 
		// END USER CODE
	}

	/**
	 * Returns a string representation of this action
	 */
	@java.lang.Override
	public java.lang.String toString()
	{
		return "convertToString";
	}

	// BEGIN EXTRA CODE
	// END EXTRA CODE
}
----

==== SCM
Par défaut Mendix utilise `SVN` pour versionner les sources du projet.
Cette gestion est masqué par l'IDE qui se charge de faire de `commit` automatiquement.
Cependant cette gestion n'est pas idéal en cas de rollback.

image::doc/images/versionning.png[img,800,557]

En cas de conflit, il peut être complexe de résoudre et fusionner les modifications

image::doc/images/versionning_01.png[img,800,431]



=== Intégration

==== RedHat SSO
Il n'y a pas de connecteur  RedHat SSO. Cependant la solution de RedHat se base sur `KeyCloak`.
KeyCloak gère différents type d'identification. Cependant la plus utilisée est l'OpenID issue 
d'`OAuth 2.0`.

Si on souhaite se connecter via le service RedHat SSO il serait nécessaire de développer 
un connecteur pour Mendix.
On retrouve une démo de connecteur `OAuth 2.0` sur le store de Mendix, cependant il n'y a pas
de documentation concernant cette démo. 


==== SAP
Il existe un connecteur SAP : https://docs.mendix.com/partners/sap/

==== OpenText
Il n'y a pas de connecteur OpenText dans Mendix, pour son intégration il est nécessaire de passer par 
un développement spécifique.

==== SolR / ElasticSearch
Il n'y a pas de connecteur pour SolR ou ElasticSearch, ces deux moteurs d'indexation peuvent 
cependant être intégrer via des appels REST. Il sera donc nécessaire de générer des entités
spécifique en fonction des appels effectués dessus.

=== Composants disponibles

=== Création de composants

Il y a deux façons de concevoir des composants.  La permière est de créer un bloc à partir d'une
structure de composants. 


image::doc/images/components_01.png[img,638,800]

Une fois créé, ce composant est également exportable. Il est ainsi possible de constuire une librairie de
composants.

La seconde façon est de créer un composant react pour la partie mobile, ou un composant Dojo pour le web.
Ces approches sont un peu plus complexe. On retrouve d'avantages d'informations sur la création de 
composant React : https://docs.mendix.com/howto/extensibility/create-a-pluggable-widget-one




=== Réutilisabilité

=== Partage inter-services
Il est possible de partager des ressources les exportants sous le fichier `mpk`. Ce fichier
est un fichier `zip` contenant un descripteur ainsi que les différentes ressources.

image::doc/images/module.png[]


On retrouve sur GitHub certains plugins de Mendix. Pour les widgets front ils utilisent
un build process sous `Gulp` pour effectuer le packaging : https://github.com/mendix/SimpleCheckboxSetSelector 

Le build process est très simple, et peut être repris pour créer de nouveau composant.

Pour les modules plus complexe pocédant du code java il n'y a pas de build process:
- https://github.com/mendix/RestServices
- https://github.com/mendix/ExcelExporter
- https://github.com/mendix/CommunityCommons/blob/master/build.gradle


=== Intégration Continue

=== Tests d'intégration

=== Suivi de production

==== Gestion des configurations

==== Monitoring

==== Gestion des logs

La gestion de log reprend le même principe que la classe `MessageFormat` en Java. Il est possible de définir un 
template de log du type `mon log {1}` avec le nombre entre accolade correspondant à une variable.
Cependant ces variables ne peuvent que être du type : Integer, Long, Boolean, Decimal, Date, Time et Enumeration. 
Les objets complexes, collections et autres ne peuvent pas être convertis directement en chaine de caractères.

image::doc/images/error_log.png[img,800,433]

Les logs par défaut ont une morphline très basic du type `{horodatage}{typologie}{level}{message}`. Il est à
noté que mendix gère le niveau de log par ligne et non par bloc de message. De ce fait en cas de stacktrace,
seule la première ligne aura le niveau `error`. On ne retrouve également pas de code d'erreur. Le log ne 
gère pas de `requestId` ou de `correlationId`. Les logs sont gérés par une librairie spécifique à Mendix (`com.mendix.logging.ILogNode`). Il n'est pas possible de customiser les logs. L'indexation dans un 
ElasticSearch n'apporterait que peut d'intéré du fait de l'impossibilité de définir les métadonnées des logs.

image::doc/images/error_log_3.png[img,800,328]

Il est possible d'ajouter des logs entre des étapes de processus :  https://docs.mendix.com/howto/monitoring-troubleshooting/log-levels 

image::doc/images/log_01.png[img,800,341]

Dans ce cas le message est modifiable:

==== Tracker front (Crashlytics)
Il n'y a pas actuellement d'intégration Crashlytics dans Mendix.


==== Déploiement

===== Blue / green
Mendix ne gère pas directement de politique de déploiement en Blue/green. Il faut donc avoir deux
environement de production avec un service WAF effectuant le blue green (F5, Cloudflare,  Amazon WAF, ...)

===== Déploiement sur les market place / déploiement progressif
Mendix ne permet pas réelement de déployer sur des stores. Il permet de lancer la construction des APK 
sur `Adobe PhoneGap`. Une fois les APKs générés le déploiement sur les stores se fait directement depuis
les stores concernés. Le déploiement progressif est donc porté par les stores et non Mendix.

Depuis l'IHM WEB (https://sprintr.home.mendix.com/index.html) on retrouve un menu pour préparer cette 
compilation. Il faut être  particulièrement vigilant à bien saisir les permissions et description de l'application sous peine de voir sa publication rejetée. Cette publication doit donc être effectuée par une personne ayant conçue 
l'application, où sachant exactement les fonctions utilisées.


image::doc/images/store.png[img,800,468]

image::doc/images/store_2.png[]



=== Les coûts à moyen terme (licences et support)

=== Compétences nécessaire
Type de ressources (coordination, support tech, business analyse, ...)


=== Resources techniques

==== Infrastructure nécessaire

==== post de développement

===== Studio Web
Mendix propose un studio web pour modifier son application. Cependant ce studio
est très limité et ne contient pas tous les widgets. Par exemple on ne peut pas
définir d'appel REST depuis l'ide WEB. 

Les équipes utilisant Mendix doivent s'oriantées vers l'IDE desktop qui offre
plus de possibilité. Le studio Web peut servir pour des modifications légères 
comme la modification de label ou repositionnement d'éléments.


===== Workspace par défaut dans les documents de l'utilisateur
Un point qui suivant les configurations machines peut être délicat, c'est que l'ide Mendix enregistre le projet courant
dans les documents de l'utilisateurs. Dans beaucoup de sociétés ce dossier est synchronisés sur des serveurs distant.
Cela peut ralentir considérablement la machine de l'utilisateur voir saturer son stockage distant.




---
== POC 


=== Navigation depuis l'IHM
L'IHM pour le POC est très simpliste. Elle consiste en une page home listant des produits. Lors
que l'on clique sur l'un d'eux on navigue vers une page de détail listant les utilisateurs ayant achetés
ce produit.

=== Service REST 
Afin de permettre de réaliser le POC, un petite application `SpringBoot` reliée à une base `Neo4j` propulse des données

image::doc/images/connect_rest/neo4j.png[img,800,511]

Cette structure de données est relativement simple, on retrouve des produits associés à des catégories et des tags.
Les différents produits sont achetés par des utilisateurs.


Afin de permettre son intégration on a deux `endpoints` :

- *http://localhost:9090/products* : qui permet la récupération des différents produits
[source,json]
----
[
    {
        "id": 4,
        "name": "mac book pro core i7 retina",
        "price": 4500.0,
        "stars": 5.0,
        "uid": 1,
        "productCategories": [
            {
                "id": 1,
                "name": "computers"
            }
        ],
        "tags": [
            {
                "id": 23,
                "name": "apple_compatibility"
            }
        ],
        "users": [
            {
                "id": 85,
                "firstName": "Aurore",
                "lastName": "Lefèbvre",
                "name": "lefebvre@rhyta.com",
                "old": 22
            },
            {
                "id": 84,
                "firstName": "Russell",
                "lastName": "Fecteau",
                "name": "russell.fecteau@rhyta.com",
                "old": 60
            },
            {
                "id": 83,
                "firstName": "Oliver",
                "lastName": "Rancourt",
                "name": "OliverRancourt@jourrapide.com",
                "old": 25
            }
        ]
    },
    {
        "id": 5,
        "name": "bird UM1",
        "price": 55.0,
        "stars": 4.0,
        "uid": 2,
        "productCategories": [
            {
                "id": 1,
                "name": "computers"
            }
        ],
        "tags": [
            {
                "id": 22,
                "name": "external_components"
            },
            {
                "id": 23,
                "name": "apple_compatibility"
            }
        ],
        "users": [
            {
                "id": 80,
                "firstName": "Durandana",
                "lastName": "Des Meaux",
                "name": "durandana.des.meaux@rhyta.com",
                "old": 85
            },
            {
                "id": 81,
                "firstName": "Aubrey",
                "lastName": "Lesage",
                "name": "AubreyLesage@rhyta.com",
                "old": 40
            }
        ]
    }
]
----
- *http://localhost:9090/products/{productId}/users* : qui permet de récupérer les utilisateurs ayant achetés le produit.
[source,json]
----
[
    {
        "id": 84,
        "firstName": "Russell",
        "lastName": "Fecteau",
        "name": "russell.fecteau@rhyta.com",
        "old": 60
    },
    {
        "id": 83,
        "firstName": "Oliver",
        "lastName": "Rancourt",
        "name": "OliverRancourt@jourrapide.com",
        "old": 25
    }
]
----





=== Etapes pour la mise en place de l'utilisation du service

. La première étape est de créer un dossier qui va contenir nos ressources. Il est très important
d'être particulièrement rigoureux sur la structuration du projet. Dans notre cas on crée un dossier
`rest` qui va contenir les différents service. Pour l'exemple qui suit, on veut afficher une page
contenant les différents utilisateurs ayant achetés un produit. Sur la home page on a une liste de 
produits, lors que l'on clique sur l'un deux on affiche la page avec les clients l'ayant acheté. 
On ajoute donc un dossier `rest/users`.
+
image::doc/images/connect_rest/connect_rest_01.png[img,800,479]

. Dans ce dossier on doit créer un `JSON structure`. Cette structure va permettre à Mendix d'identifier
les différents objets et typages dont est constitués la réponse de notre service. Cette structure doit
être modifiée à chaque fois que la réponse de notre service est modifiée.
+
image::doc/images/connect_rest/connect_rest_02.png[img,800,479]

. Dans le champs playload il faut coller une réponse type de notre service. Une fois renseigné,
il faut rafraichir la structure pour que Mendix analyse le JSON.
+
image::doc/images/connect_rest/connect_rest_03.png[img,800,479]

. La structure JSON n'est pas directement utilisable. Pour que Mendix puisse l'utiliser,
il faut créer un mapping, pour cela nous devons ajouter une `import mapping`
+
image::doc/images/connect_rest/connect_rest_04.png[img,800,479]

. Ce mapping doit faire référence à la structure JSON que l'on vient de créer. Les différents champs
s'affiche et il faut sélectionner l'intégralité de ces derniers.
+ 
image::doc/images/connect_rest/connect_rest_05.png[img,800,479]

. Le mapping permet d'affecter des entités au sens Mendix avec la structure en provenance du service 
rest. Un bouton nous permet de mapper automatiquement. Il est à noter qu'en cas de changement dans la
réponse de notre service, ces actions sont également à refaire.
+
image::doc/images/connect_rest/connect_rest_06.png[img,800,479]

. Le mapping va générer des entités avec des noms génériques. Il faut bien penser à aller dans
la partie `Domain Model` pour renomer les différentes entités
+
image::doc/images/connect_rest/connect_rest_07.png[img,800,479]
image::doc/images/connect_rest/connect_rest_08.png[img,800,479]

. Afin d'invoquer notre service REST il faut créer un `microflow` avec un nom explicite. Dans notre
exemple on le nomme `getUsersByProduct`. Dans le cas du POC, une approche très simple est mise en
place. En réalité tous processus devrait être conçu suivant la programmation par contract. Ce
type de développement impose des pré-conditions et post-condition sur l'ensemble des processus
afin de garantir l'intégrité des données. De même tout appels sortant doit générer des logs
spécifiques (partner log) afin de suivre en production ces appels et d'avoir des KPI sur ces derniers.
+
image::doc/images/connect_rest/connect_rest_09.png[img,800,479]

. Le microflow aura besion d'un paramètre d'entrée qui est le produit conserné par la recherche.
Pour concevoir l'URL du service, seul l'identifiant est nécessaire. Cependant il n'est pas possible
d'envoyer uniquement cette information dans le microflow depuis la navigation actuelle.
+
image::doc/images/connect_rest/connect_rest_10.png[img,800,479]

. Pour invoquer notre service REST il faut ajouter une action du type `Call REST service`. Dans le configuration de l'action, on peut saisir l'URL d'appel en cliquant sur `Edit`. Une nouvelle
fenêtre s'ouvre avec un champs pour le template de l'URL et une seconde partie pour définir les variables
à injecter. Ici le paramètre variant est l'id du produit, on saisi `{1}` dans l'URL ce qui va nous
permettre de définir une expression pour extraire l'id du produit (`toString($product/id)`). L'id
du produit est une variable de type `long`, les URL sont des chaînes de caractères, il est obligatoire
dans Mendix de convertir le typage avec une fonction Java, dans notre cas le 'toString'. Cette approche
est très étonnante, car des objets Java comme le `MessageFormat` permettent d'effectuer ce templating
sans qu'il ne soit nécessaire de gérer cette conversion de type. Cette classe utilitaire utilise 
différent formatteur pour effectuer ce traitement. Petit point d'attention sur le pointage sur les
attribut qui s'effectue avec le caractère `/`.
+
image::doc/images/connect_rest/connect_rest_11.png[img,800,479]

. Une fois la configuration de l'URL effectuée, il faut sélectionner un mappeur pour notre action et
définir le nom de la variable que l'action produira. Dans notre cas `users`
+
image::doc/images/connect_rest/connect_rest_13.png[img,800,479]

. Pour l'instant l'objet n'est pas reconnu comme liste par Mendix, il est nécessaire de
récupérer la variable et la convertir. Pour cela on ajoute une action `Retrive` qui va
se charger de ce transtypage.
+
image::doc/images/connect_rest/connect_rest_14.png[img,800,479]
image::doc/images/connect_rest/connect_rest_15.png[img,800,479]

. Pour finir avec le microflow, il faut définir la sortie du process.
+
image::doc/images/connect_rest/connect_rest_16.png[img,800,479]

. Une fois le microflow créé, on peut ajouter une nouvelle page. Ici on prend un template de page
vide.
+
image::doc/images/connect_rest/connect_rest_17.png[img,800,479]

.  Cette page va recevoir dans son contexte le produit (`MyFirstModule.Product`). Il faut pour
cela ajouter une `Data view` pour receptionner la valeur
+
image::doc/images/connect_rest/connect_rest_18.png[img,800,479]

. Pour afficher la liste des utilisateurs on utilise le composant `List view`. Ce composant peut 
invoquer un microflow pour son initialisation.
+
image::doc/images/connect_rest/connect_rest_19.png[img,800,479]

. Afin de transférer la valeur du produit, on passe également par un microflow. Ce processus va
récupérer en entrée le produit, procéder à la navigation
+
image::doc/images/connect_rest/connect_rest_20.png[img,800,479]

. Il faut également configurer la `List view` présente sur la home page pour pouvoir déclacher
le microflow et ainsi gérer la navigation
+
image::doc/images/connect_rest/connect_rest_22.png[img,800,479]

. Une fois déployé on peut effectuer la navigation
+
image::doc/images/connect_rest/connect_rest_21.png[img,800,479]
image::doc/images/connect_rest/connect_rest_22.png[img,800,479]


Ce que l'on constate est que la navigation s'effectue en `POST`. En effet dans ce mode de
fonctionnement le système n'a pas la possiblité de transférer d'information via l'URL de navigation.
Cette approche, bien que fonctionnelle, pose des problème de `SEO`. Il n'est pas possible de 
naviguer directement sur la page. En terme de référencement celà peut être problématique.
Sur une application mobile, le problème est moindre car on n'utilise pas d'URL direct.

Cependant dans les deux cas il peut être nécessaire de pouvoir accéder directement à une page (deep link). Un exemple de plugin existe, cependant ce dernier n'est pas des plus évident à mettre en place.

Autre point négatif est la résilience du système. Il est particulièrement complexe de faire du
développement par contrats. Les microflow ne sont pas réelement conçu pour. Les cas d'echec de processus
ne sont pas simple à mettre en place. Ces derniers ne peuvent être conçu que depuis des sous processus.
Si on souhaite avoir une vraie gestion d'exception, cela passe obligatoirement par l'implémentation 
d'une action Java.

De même les connecteurs ne sont pas tracer ni conçu pour le suivi de production. 

=== Définition de style CSS spécifique
Afin de définir une charte graphique, il est nécessaire d'exporter son projet pour eclipse. Ce processus
va juste préparer le code source du projet afin qu'il soit éditable. Une fois exporté on peut ajouter un 
fichier custom.css dans le dossier theme/style/web/css. Ce dossier est définit par convention, on ne peut
pas avoir le choix sur son architecture.

.theme/styles/web/css/custom.css
[source,css]
----
.user-lastname{
    margin-left: 1em;
}
----

---
== Avantages & inconvénients


=== Avantages

- La contribution sur la mise en page est simple et rapide
- Beaucoup de composants de bases existant

=== inconvénients

